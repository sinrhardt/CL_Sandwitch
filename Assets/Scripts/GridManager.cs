using System;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerMove
{
    public PlayerMove(List<Tile> listTakenFrom, List<Tile> listGivenTo, Vector3 direction, int tilesMoved)
    {
        ListTakenFrom = listTakenFrom;
        ListGivenTo = listGivenTo;
        Direction = direction;
        TilesMoved = tilesMoved;
    }

    public List<Tile> ListTakenFrom;
    public List<Tile> ListGivenTo;
    public Vector3 Direction;
    public int TilesMoved;
}


public class GridManager : MonoBehaviour
{
    [SerializeField] private int size = 4;
    [SerializeField] private Transform tileFolder;
    [SerializeField] private GameObject breadTilePrefab;
    [SerializeField] private List<GameObject> ingredientTilePrefab;
    
    private List<Tile>[,] _grid;
    private int _currentTiles;
    private List<Tile> _firstBreadList;
    private List<Tile> _secondBreadList;

    private LinkedList<PlayerMove> _movesHistory;
    

    public static Action OnReset;
    public static Action OnPlayerWin;
    
    private void OnEnable()
    {
        UI_Logic.OnUndo += UndoMove;
        UI_Logic.OnReset += ResetGrid;

        Tile.OnMoveBread += CheckWinCondition;
    }

    private void OnDisable()
    {
        UI_Logic.OnUndo -= UndoMove;
        UI_Logic.OnReset -= ResetGrid;
        
        Tile.OnMoveBread -= CheckWinCondition;
    }

    private void Start()
    {
        _movesHistory = new LinkedList<PlayerMove>();
        InitGrid();

        PlaceRandomTiles(6);
    }

    #region TileMovement
    
    public bool CanMove(Vector3 position, Vector3 direction)
    {
        var currentIndexI = (int) position.x / 10;
        var currentIndexJ = (int) position.z / 10;

        // If going out of bounds return
        if (currentIndexI + direction.x >= size || 
            currentIndexI + direction.x < 0 ||
            currentIndexJ + direction.z >= size ||
            currentIndexJ + direction.z < 0)
            return false;
        
        // If going on empty list
        if (_grid[currentIndexI + (int)direction.x, currentIndexJ + (int)direction.z].Count == 0)
            return false;

        int tilesMoved;
        
        var listGivenTo = _grid[currentIndexI + (int)direction.x, currentIndexJ + (int)direction.z];
        var listTakenFrom = _grid[currentIndexI, currentIndexJ];

        var toMove = listTakenFrom.Count;
        for (tilesMoved = 0; tilesMoved < toMove; tilesMoved++)
        {
            var highest = GetHighest(listTakenFrom);
            listGivenTo.Add(highest);
            listTakenFrom.Remove(highest);
            highest.MoveTile(direction,listGivenTo.IndexOf(highest));
        }
        
        listTakenFrom.Clear();
        

        PlayerMove currentMove = new PlayerMove(listTakenFrom , listGivenTo, direction, tilesMoved);
        _movesHistory.AddLast(currentMove);
        
        return true;
    }

    private void UndoMove()
    {
        if (_movesHistory.Count == 0) return;
        
        PlayerMove move = _movesHistory.Last.Value;
        for (var i = 0; i < move.TilesMoved; i++)
        {
            var highest = GetHighest(move.ListGivenTo);
            
            move.ListTakenFrom.Add(highest);
            move.ListGivenTo.Remove(highest); 
            highest.MoveTile(move.Direction * -1, move.ListTakenFrom.IndexOf(highest), true);
            
        }
        _movesHistory.RemoveLast();
    }
    
    private static Tile GetHighest(IReadOnlyList<Tile> list)
    {
        var temp = list[0];
        for (var i = 1; i < list.Count; i++)
        {
            if (list[i].transform.position.y > temp.transform.position.y)
            {
                temp = list[i];
            }
        }

        return temp;
    }
    
    private static Tile GetLowest(IReadOnlyList<Tile> list)
    {
        var temp = list[0];
        for (var i = 1; i < list.Count; i++)
        {
            if (list[i].transform.position.y < temp.transform.position.y)
            {
                temp = list[i];
            }
        }

        return temp;
    }
    
    

    #endregion

    #region GridManagement
    
    private void InitGrid()
    {
        _grid = new List<Tile>[size, size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                _grid[i, j] = new List<Tile>();
            }
        }
    }

    private void ClearGrid()
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                _grid[i, j].Clear();
            }
        }
    }

    private void ResetGrid()
    {
        ClearGrid();
        _movesHistory.Clear();
        OnReset?.Invoke();
    }
    
    
    #endregion

    #region TileSetup
    
    private void InstantiateTile(GameObject prefab, Vector2 position)
    {
        var temp = Instantiate(prefab, new Vector3(position.x * 10, 0, position.y * 10), Quaternion.identity, tileFolder).GetComponent<Tile>();
        temp.gridManager = this;
        temp.initialList = _grid[(int)position.x, (int)position.y];
        _grid[(int) position.x,(int) position.y].Add(temp);
    }

    private void PlaceRandomTiles(int ingredientsNumber)
    {
        // Bread
        var point1 = new Vector2(Random.Range(0, size - 1), Random.Range(0, size - 1));
        var point2 = point1 + GetRandomAdjacent(point1);
        
        InstantiateTile(breadTilePrefab, point1);
        InstantiateTile(breadTilePrefab, point2);

        _firstBreadList = _grid[(int)point1.x,(int)point1.y];
        _secondBreadList = _grid[(int)point2.x,(int)point2.y];
        
        _currentTiles += 2;
        
        var currentPivot = point1;
        for (int i = 0; i < ingredientsNumber/2; i++)
        {
            var adjacent = GetRandomAdjacent(currentPivot);
            if (adjacent.x == -1) break;
            var newPoint = currentPivot + adjacent;
            InstantiateTile(ingredientTilePrefab[Random.Range(0,3)], newPoint);
            _currentTiles++;
            currentPivot = newPoint;
        }
        
        currentPivot = point2;
        for (int i = 0; i < ingredientsNumber/2; i++)
        {
            var adjacent = GetRandomAdjacent(currentPivot);
            if (adjacent.x == -1) break;
            var newPoint = currentPivot + adjacent;
            InstantiateTile(ingredientTilePrefab[Random.Range(0,3)], newPoint);
            _currentTiles++;
            currentPivot = newPoint;
        }
        
    }

    private bool IsOccupied(Vector2 pos)
    {
        if (pos.x < 0 || pos.x >= size || pos.y < 0 || pos.y >= size) return true;
        return _grid[(int)pos.x,(int)pos.y].Count > 0;
    }

    private Vector2 GetRandomDirection()
    {
        return (Direction)Random.Range(0, 3) switch
        {
            Direction.Up => Vector2.up,
            Direction.Right => Vector2.right,
            Direction.Left => Vector2.left,
            Direction.Down => Vector2.down,
        };
    }

    private Vector2 GetRandomAdjacent(Vector2 point1)
    {
        var point2 = GetRandomDirection();
        if (!IsOccupied(point1 + point2))
            return point2;
        
        
        for (int tries = 0; IsOccupied(point1 + point2); tries++)
        {
            point2 = GetRandomDirection();
            if (tries > 50)
                return new Vector2(-1, -1);
        }

        return point2;
    }
    
    #endregion

    private void CheckWinCondition()
    {
        if (_firstBreadList.Count != _currentTiles && _secondBreadList.Count != _currentTiles)
        {
            UndoMove();
            return;
        }

        var list = _firstBreadList.Count == _currentTiles ? _firstBreadList : _secondBreadList;
        if (!GetHighest(list).IsBread || !GetLowest(list).IsBread)
        {
            UndoMove();
            return;
        }
        
        
        OnPlayerWin?.Invoke();
        
    }
    
}
