using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class Tile : MonoBehaviour
{
    [SerializeField] private bool isBread;
    [SerializeField] private Transform mesh;
    private bool _canMove = true;

    public GridManager gridManager;
    public List<Tile> initialList;
    private Vector3 _initialPos;

    public bool IsBread => isBread;

    
    public static Action OnMoveBread;
    
    private void Start()
    {
        _initialPos = transform.position;
    }

    private void OnEnable()
    {
        GridManager.OnReset += ResetPosition;
    }

    private void OnDisable()
    {
        GridManager.OnReset -= ResetPosition;
    }

    private void ResetPosition()
    {
        if (!_canMove) return;
        
        initialList.Add(this);
        transform.DOJump(_initialPos, Random.Range(15, 35), 1, 1.0f, true).OnComplete(() => _canMove = true);
        mesh.DORotate(Vector3.one * 360 * Random.Range(1, 5), 1.0f, RotateMode.LocalAxisAdd).OnComplete(() => mesh.rotation = new Quaternion(0, 0, 0, 0));

    }

    public void MoveInput(Direction direction)
    {
        if (!_canMove) return;

        var moveV3 = direction switch
        {
            Direction.Up => Vector3.forward,
            Direction.Right => Vector3.right,
            Direction.Left => Vector3.left,
            Direction.Down => Vector3.back,
        };

        if (gridManager.CanMove(transform.position, moveV3)) return;
        
        _canMove = false;
        mesh.DOShakePosition(0.5f).OnComplete(() => _canMove = true);
        
    }

    public void MoveTile(Vector3 direction, int height)
    {
        _canMove = false;
        
        Vector3 rotateDir;
        
        if (direction == Vector3.up)
            rotateDir = Vector3.right;
        else if (direction == Vector3.right)
            rotateDir = Vector3.back;
        else if (direction == Vector3.left)
            rotateDir = Vector3.forward;
        else
            rotateDir = Vector3.left;
        
        var endPos =transform.position + direction * 10;
        endPos.y = height * 2;
        mesh.DORotate(rotateDir * 180, 0.5f, RotateMode.LocalAxisAdd);
        transform.DOJump(endPos, 15.0f, 1, 0.5f, true).OnComplete(OnAnimationComplete);
        
    }
    
    public void MoveTile(Vector3 direction, int height, bool undo)
    {
        
        _canMove = false;
        
        Vector3 rotateDir;
        
        if (direction == Vector3.up)
            rotateDir = Vector3.right;
        else if (direction == Vector3.right)
            rotateDir = Vector3.back;
        else if (direction == Vector3.left)
            rotateDir = Vector3.forward;
        else
            rotateDir = Vector3.left;
        
        var endPos =transform.position + direction * 10;
        endPos.y = height * 2;
        mesh.DORotate(rotateDir * 180, 0.5f, RotateMode.LocalAxisAdd).OnComplete((() => mesh.Rotate(0,0,0)));
        transform.DOJump(endPos, 15.0f, 1, 0.5f, true).OnComplete(() => _canMove = true);
        
    }

    private void OnAnimationComplete()
    {
        mesh.rotation = Quaternion.identity;
        _canMove = true;
        
        if (isBread) OnMoveBread?.Invoke();
    }
    
    

}
