using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    void Start()
    {
        SceneManager.LoadScene("UIScene", LoadSceneMode.Additive);
        SceneManager.LoadScene("StageScene", LoadSceneMode.Additive);
    }

    private void OnEnable()
    {
        GridManager.OnPlayerWin += OnPlayerWin;
    }
    
    private void OnDisable()
    {
        GridManager.OnPlayerWin -= OnPlayerWin;
    }

    private void OnPlayerWin()
    {
        StartCoroutine(LoadNextStage());
    }

    private IEnumerator LoadNextStage()
    {
        yield return new WaitForSeconds(1.0f);
        
        SceneManager.UnloadSceneAsync("StageScene");
        SceneManager.LoadScene("StageScene", LoadSceneMode.Additive);
    }
    


}
