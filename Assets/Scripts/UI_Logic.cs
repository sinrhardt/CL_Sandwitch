using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UI_Logic : MonoBehaviour
{
    [SerializeField] private CanvasGroup fade;
    
    
    public static Action OnUndo;
    public static Action OnReset;

    private bool _canClick = true;

    private void OnEnable()
    {
        GridManager.OnPlayerWin += OnPlayerWin;
    }
    
    private void OnDisable()
    {
        GridManager.OnPlayerWin -= OnPlayerWin;
    }

    private void OnPlayerWin()
    {
        _canClick = false;
        fade.blocksRaycasts = true;
        fade.DOFade(1, 1.0f).OnComplete(() => _canClick = true);
    }

    public void UndoMove()
    {
        Debug.Log("Undo");
        OnUndo?.Invoke();
    }
    
    public void ResetGrid()
    {
        Debug.Log("Reset");
        OnReset?.Invoke();
    }

    public void FadeAway()
    {
        if (!_canClick) return ;
        fade.blocksRaycasts = false;
        fade.DOFade(0, 1.0f);
    }
}
