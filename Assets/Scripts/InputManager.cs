using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private LayerMask tileLayer;
    [SerializeField] private float inputDelta = 1.5f;

    private Tile _selectedTile;
    private Vector2 _startPos;
    
    void Update()
    {
        InputLogic();
    }

    private void InputLogic()
    {
        if (Input.touchCount != 1) return;
        
        var touch = Input.GetTouch(0);

        switch (touch.phase)
        {
            case TouchPhase.Began:
            {
                var ray = Camera.main.ScreenPointToRay(touch.position);
                if (!Physics.Raycast(ray.origin, ray.direction, out var hit, Mathf.Infinity, tileLayer))
                    return;

                _selectedTile = hit.transform.GetComponent<Tile>();
                _startPos = touch.position;
                break;
            }
            case TouchPhase.Moved:
            {
                Vector2 diff = _startPos - touch.position;
                if (diff.magnitude < inputDelta || _selectedTile == null) return;
                MoveTile(diff);
                break;
            }
            case TouchPhase.Ended:
                _selectedTile = null;
                break;
        }
    }

    private bool _canSubmit = true;
    private void MoveTile(Vector2 diff)
    {
        if (!_canSubmit) return;
        _canSubmit = false;
        
        Direction output;
        if (Mathf.Abs(diff.x) >= Mathf.Abs(diff.y))
        {
            output = diff.x >= 0 ? Direction.Left : Direction.Right;
        }
        else
        {
            output = diff.y >= 0 ? Direction.Down : Direction.Up;
        }
        _selectedTile.MoveInput(output);
        transform.DORotate(Vector3.zero, 1f).OnComplete(() => _canSubmit = true);
    }
}
